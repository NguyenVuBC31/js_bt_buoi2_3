// JS bài tập 1 Start
/*
Bài tập 1:
Input: Tiền lương một ngày: 100000
        Số ngày đi làm: 5
Các bước thực hiện:
B1: Nhập tiền lương 1 ngày (100000)
B2: Nhập số ngày đi làm: 5
B3: Tính số tiền lương
    Công thức tính lương: Lương 1 ngày * số ngày làm
B4: In kết quả ra console

Output: Số tiền lương: 500000

*/
document.getElementById("txt-tinh-luong").addEventListener("click",
function(){
    var tienLuong1Ngay = document.getElementById("txt-luong-mot-ngay").value*1;
    var soNgayLam = document.getElementById("txt-so-ngay").value*1;
    var result = null;
    
    result= tienLuong1Ngay*soNgayLam;

    document.getElementById("txt-tien-luong").innerHTML=
    `
    <div>
    Tiền Lương là: ${result} $
    </div>
    `
}
)
// JS bài tập 1 End

// JS bài tập 2 start

/*
Bài tập 2:
Input: Số thực 1: 3
       Số thực 2: 6
       Số thực 3: 7
       Số thực 4: 4
       Số thực 5: 8
Các bước thực hiện:
B1: Tạo biến và gán giá trị cho số thực 1 : 3
    Tạo biến và gán giá trị cho số thực 2 : 6
    Tạo biến và gán giá trị cho số thực 3 : 7
    Tạo biến và gán giá trị cho số thực 4 : 4
    Tạo biến và gán giá trị cho số thực 5 : 8
B2: Tính giá trị trung bình bằng tổng 5 số thực chia cho 5 : (số thực 1 + số thực 2 + số thực 3 + số thực 4 + số thực 5)/5 
B3: In kết quả ra console: 3 + 6 + 7 + 4 + 8 = 5.6;       
Output: Trung bình cộng 5 số thực là 5.6
*/
document.getElementById("txt-tinh-trung-binh").addEventListener("click",
function(){
    var num1 = document.getElementById("txt-so-1").value*1;
    var num2 = document.getElementById("txt-so-2").value*1;
    var num3 = document.getElementById("txt-so-3").value*1;
    var num4 = document.getElementById("txt-so-4").value*1;
    var num5 = document.getElementById("txt-so-5").value*1;
    var result2 = null;

    result2 = (num1 + num2 + num3 + num4 + num5) / 5;

    document.getElementById("txt-gia-tri-tb").innerHTML=
    `
    <div>
    Giá trị trung bình là: ${result2}
    </div>
    `
}
);
// JS bài tập 2 end

// JS bài tập 3 start
/*
Bài tập 3:
Input: Nhập giá USD hiện nay: 1 USD = 23.500VND;
    Nhập vào số tiền USD : 3 USD
Các bước thực hiện:
B1: Tạo biến và gán giá trị giá USD hiện nay
    Tạo biến và gán giá trị cho số tiền USD cần quy đổi : 3 USD
B2: Tính giá tiền quy đổi từ USD sang VND: Giá USD hiện nay * số tiền USD 
B3: in kết quả ra console: 23.500*3
Output: 3 USD sau khi quy đổi sang VND là 70.500VND
*/
document.getElementById("txt-quy-doi").addEventListener("click",
function(){
    const giaUSD = 23500;
    var usdQuyDoi = document.getElementById("txt-tien-usd").value*1;
    var result3 = null;

    result3 = giaUSD*usdQuyDoi;
document.getElementById("txt-tien-vnd").innerHTML=
`<div>
    Số tiền là: ${result3} VNĐ
</div>`
}
);
// JS bài tập 3 end

// JS bài tập 4 start
/*
Bài Tập 4:
Input: Độ dài cạnh a = 3
       Độ dài cạnh b = 4
Các bước thực hiện:
B1: Tạo biến và gán giá trị cạnh a
    Tạo biến và gán giá trị cạnh b
B2: Áp dụng công thức tính diện tích hình chữ nhật : cạnh a * cạnh b 
    Áp dụng công thức tính chu vi hình chữ nhật: (cạnh a + cạnh b) * 2
B3: In kết quả ra console
Output: Diện tích hình chữ nhật là 12
        Chu vi hình chữ nhật là 14
*/
document.getElementById("txt-tinh-dtcv").addEventListener("click",
function(){
    var chieuDai = document.getElementById("txt-chieu-dai").value*1;
    var chieuRong = document.getElementById("txt-chieu-rong").value*1;
    var dienTich = null;
    var chuVi = null;

    dienTich = chieuDai*chieuRong;
    chuVi = (chieuDai+chieuRong)*2;

    document.getElementById("txt-dtcv").innerHTML=
    `<div>
        Diện tích hình chữ nhật là: ${dienTich} cm </br>
        Chu vi hình chữ nhật là: ${chuVi} cm
    </div>`
}
);
// JS bài tập 4 end


// JS bài tập 5 start
/*
Bài tập 5:
Input: Nhập số có 2 chữ số : 33
Các bước thực hiện:
B1: Tạo và gán giá trị cho số có 2 chữ số: 33
B2: Lấy số hàng chục: Số / 10
    Lấy số hàng đơn vị: Số % 10
B3: Tính tổng số hàng chục và số hàng đơn vị : số hàng chục + số hàng đơn vị
B4: In kết quả ra console
Output: Tổng 2 ký số = 6
*/
document.getElementById("txt-tinh-tong").addEventListener("click",
function(){
    var numb = document.getElementById("txt-so").value*1;
var soHangChuc = null;
var soHangDonVi = null;
var sum= null;

soHangChuc= Math.floor( numb / 10 );
soHangDonVi= numb%10;
sum= soHangChuc+soHangDonVi;

document.getElementById("txt-tong").innerHTML=
`<div>
Tổng ký số là: ${sum}
</div>`
}
);

// JS bài tập 5 end